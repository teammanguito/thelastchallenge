import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import { routing, appRoutingProviders } from "./app.routes";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { TransactionComponent } from "./components/transaction/transaction.component";
import { HeaderComponent } from "./components/shared/header/header.component";
import { FooterComponent } from "./components/shared/footer/footer.component";
import { HomeComponent } from "./components/home/home.component";
import { AboutComponent } from "./components/shared/about/about.component";

// Services

import { TransactionService } from "./services/transaction.service";
import { UsersService } from "./services/users.service";
import { PanelComponent } from './components/panel/panel.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TransactionComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    PanelComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [TransactionService, appRoutingProviders, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule {}
