import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// import { NgModule } from "@angular/core";

import { TransactionComponent } from "./components/transaction/transaction.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { AboutComponent } from "./components/shared/about/about.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";

const routes: Routes = [
  { path: "transaction", component: TransactionComponent },
  { path: "login", component: LoginComponent, pathMatch: "full" },
  { path: "about", component: AboutComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "", component: HomeComponent }
];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class FeatureRoutingModule {}

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
