import { Injectable } from "@angular/core";
import { UserModel } from "../models/users";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { GLOBAL_BACKEND } from "../global";

@Injectable({
  providedIn: "root"
})
export class UsersService {
  url: string;
  public identity;
  private endpoint: string = "users";

  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL_BACKEND;
  }

  getUserFromAPI(user: string): Observable<any> {
    let header = new HttpHeaders();
    const endPoint = `${this.url}/${this.endpoint}/${user || ""}`;

    header.set("Content-Type", "application/json");
    header.set("accept", "application/json");
    let data = this._httpClient.get(endPoint, { headers: header });
    return data;
  }

  signin(user): Observable<any> {
    // if(gettoken != null){
    //   user.gettoken = gettoken;

    // }
    let params = JSON.stringify(user);
    let header = new HttpHeaders();
    header.set("Content-Type", "application/json");
    return this._httpClient.post(this.url + "/login/", params, {
      headers: header
    });
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem("identity"));
    if (identity != "undefined") {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;
  }
}

// export interface User {
//   nombre: string;
//   bio: string;
//   img: string;
//   aparicion: string;
//   casa: string;
// }
