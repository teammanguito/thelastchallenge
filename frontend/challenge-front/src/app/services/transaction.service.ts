import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { GLOBAL_BACKEND } from "../global";

@Injectable()
export class TransactionService {
  public url: string;
  private endpoint: string = "clienttransaction";
  private transactions: Transaction[];
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL_BACKEND;
  }

  getTransaction(): Observable<any> {
    let header = new HttpHeaders();
    const endPoint = `${this.url}/${
      this.endpoint
    }/?init=2015-01-01&txnType=a&end=2018-12-31`;
    console.log(endPoint);

    header.set("Content-Type", "application/json");
    let data = this._httpClient.get(endPoint, { headers: header });
    console.log(data);

    return data;
  }
}

export interface Transaction {
  fecha: Date;
  clientId: number;
  txn: number;
}
