import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import {
  TransactionService,
  Transaction
} from "../../services/transaction.service";

@Component({
  selector: "app-transaction",
  templateUrl: "./transaction.component.html",
  styleUrls: ["./transaction.component.css"]
})
export class TransactionComponent implements OnInit {
  transaction: Transaction[];

  constructor(
    private _router: Router,
    private _activateRoute: ActivatedRoute,
    private _transactionService: TransactionService
  ) {
    console.log(_transactionService.url);
  }

  ngOnInit() {
    // this._transactionService.getTransaction().subscribe(response => {
    //   if (response) {
    //     console.log(response);
    //   }
    // });
    // console.log(this._transactionService.getTransaction());
  }
}
