import { Component, OnInit } from "@angular/core";
import { UsersService } from "../../services/users.service";
import { UserModel } from "../../models/users";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  users: UserModel[] = [];
  constructor(private _usersService: UsersService) {}

  ngOnInit() {
    // this.users = this._usersService.getUsers();
    // console.log(this.users);
    this._usersService.getUserFromAPI("").subscribe(response => {
      if (response) {
        console.log(response.result);
        this.users = response.result;
      }
    });
    console.log(this._usersService.getUserFromAPI(""));
  }
}
