import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { UsersService } from "../../../services/users.service";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  public title: string;
  public identity: string;
  public logIn: boolean;
  constructor(private _usersService: UsersService, private _router: Router) {
    this.identity = this._usersService.getIdentity();
  }

  ngOnInit() {
    this.identity = this._usersService.getIdentity();
    this.logIn = this.identity ? true : false;
    // console.log(this.identity);
    // console.log(this.logIn);
  }
  ngDoCheck() {
    this.identity = this._usersService.getIdentity();
  }
  logout() {
    localStorage.clear();
    this.identity = null;
    this._router.navigate(["/"]);
  }
}
