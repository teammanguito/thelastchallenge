# -*- coding: utf-8 -*-
import tornado
from tornado import httpserver
from tornado.wsgi import WSGIContainer
from tornado.ioloop import IOLoop
from tornado.web import FallbackHandler, RequestHandler, Application
from tornado.httpclient import AsyncHTTPClient
import motor.motor_tornado
from app import app
from config import appConfig
from app.api.clientTransaction.clientTransaction import ClientTransactionHandler
# from app.services.hdf import db

# ----------------------------
# hdf5 database file
# ----------------------------
# data = db.loadDataBase()
# data.load()
# clientDB = data.retrieve()
# ----------------------------

conf = appConfig()
port = conf.getConfigByEnvironment('port')
address = conf.getConfigByEnvironment('address')
url, database = conf.getMongoUrl()
client = motor.motor_tornado.MotorClient(url)

db = client[database]

wsgi_app = WSGIContainer(app)

application = Application([
    (r'/clienttransaction/(.*)', ClientTransactionHandler),
    (r".*", FallbackHandler, dict(fallback=wsgi_app)),
], db=db)

if __name__ == "__main__":
    if port is None or address is None:
        print('you cannot run the server. check the config file')
    else:
        print('The full URL is http://{address}:{port}'.format(port=port, address=address))
        application.listen(port, address=address)
        print(database + 'db selected!')
        print(db)
        print('The server running in {port}.'.format(port=port))
        sockets = tornado.netutil.bind_sockets(8888)
        tornado.process.fork_processes(0)
        server = httpserver.HTTPServer(application)
        # server.add_sockets(sockets)
        IOLoop.current().start()
        # IOLoop.instance().start()