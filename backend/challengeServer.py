from tornado import (
    auth,
    escape,
    httpserver,
    ioloop,
    options,
    web
)
from tornado.options import define, options
import threading
import motor.motor_tornado
import asyncio

import Settings
from config import appConfig

conf = appConfig()

define("port", default=conf.getConfigByEnvironment('port'), help="server port", type=int)
define("host", default=conf.getConfigByEnvironment('address'), help="host of project", type=str)
url, database = conf.getMongoUrl(conf.getConfigByEnvironment('source'))

class Application(web.Application):
    def __init__(self):
        print('init')
        print(url, database)
        print('-' * 20)
        client = motor.motor_tornado.MotorClient(url)
        db = client[database]
        handlers = [
            # (r'/clienttransaction/(.*)', ClientTransactionHandler)
        ]
        settings = {
            'db': db,
            'MAX_WORKERS': Settings.MAX_WORKERS,
            'limit': 50000,
            'debug': Settings.DEBUG,
            
        }
        web.Application.__init__(self, handlers, **settings)

def main():
    app = Application()
    server = httpserver.HTTPServer(app)
    server.bind(8888)
    server.start(0)  # Forks multiple sub-processes
    ioloop.IOLoop.current().start()

main()