from urllib.parse import quote_plus, quote
import Settings

class appConfig:
    """
    Clase que contiene la configuración de valores globales de app
    """
    CONFIG_VALUES = {
        'global': {
            'port': 3500,
            'address': '0.0.0.0',
            'currentEnv': 'debug',
            'source': 'local'
        },
        'mongo-local': {
            'host': 'localhost',
            'port': '27017',
            'dbname': 'challengedb',
            'user': '',
            'pswd': '',
            'auth': False
        },
        'mongo-cloud': {
            'host': 'ds044587.mlab.com',
            'port': '44587',
            'dbname': 'challengedb',
            'user': 'jyanez',
            'pswd': '#Jose1301257.',
            'auth': True
        }

    }


    def getConfigByEnvironment(self, key):
        env = 'global'
        try:
            if not isinstance(key, str) or key == '':
                raise('the key value is not valid.')
                # return None

            value = self.CONFIG_VALUES[env].get(key, None)

            return value
        except Exception as ex:
            print(ex)
            return None


    def getMongoUrlParameter(self, locationSource: str = 'local'):
        if locationSource == 'local':
            return self.CONFIG_VALUES['mongo-local'].get('host', None)
        elif locationSource == 'cloud':
            return self.CONFIG_VALUES['mongo-cloud'].get('host', None)
        else:
            return None

    def getMongoUrl(self, locationSource: str = 'local'):
        try:
            env = 'mongo-{}'.format(locationSource)
            if locationSource == 'local':
                confConn = self.CONFIG_VALUES[env]
            else:
                confConn = self.CONFIG_VALUES[env]
                
            if confConn['auth'] == False:
                urlConnTemplate = 'mongodb://{host}:{port}'
                urlConnBuild = urlConnTemplate.format(
                    host=confConn['host'],
                    port=confConn['port'],
                )
            else:
                urlConnTemplate = 'mongodb://{user}:{pswd}@{host}:{port}'
                urlConnBuild = urlConnTemplate.format(
                    pswd=quote(confConn['pswd']),
                    user=quote(confConn['user']),
                    host=confConn['host'],
                    port=confConn['port'],
                )

            return urlConnBuild, confConn['dbname']
        except Exception as ex:
            print('getMongoUrl - Error')
            print(ex)
            return None, None
        
