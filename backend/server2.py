from tornado import (
    auth,
    escape,
    httpserver,
    ioloop,
    options,
    web
)
import threading
import motor.motor_tornado
import asyncio

from app import app
from config import appConfig
from app.api.clientTransaction.clientTransaction import ClientTransactionHandler
from app.api.users.users import UserHandler
from app.api.users.login import LoginHandler

conf = appConfig()
port = conf.getConfigByEnvironment('port')
address = conf.getConfigByEnvironment('address')
url, database = conf.getMongoUrl(conf.getConfigByEnvironment('source'))

# app = None

class Application(web.Application):
    def __init__(self):
        print('init')
        print(url, database)
        print('-' * 20)
        client = motor.motor_tornado.MotorClient(url)
        db = client[database]
        handlers = [
            (r'/clienttransaction/(.*)', ClientTransactionHandler),
            (r'/users/(.*)', UserHandler),
            (r'/login/(.*)', LoginHandler),
        ]
        settings = {
            'db': db,
            'MAX_WORKERS': 4,
            'limit': 50000,
            'debug': True
        }
        web.Application.__init__(self, handlers, **settings)

class ChallengeServer(threading.Thread):
    def run(self):
        try:
            if port is None or address is None:
                raise('you cannot run the server. check the config file')
            else:
                print('The full URL is http://{address}:{port}'.format(port=port, address=address))
                asyncio.set_event_loop(asyncio.new_event_loop())
                options.parse_command_line()
                app = Application()
                db = app.settings['db']
                server = httpserver.HTTPServer(app) # Non-blocking execution server
                server.listen(port)
                server.bind(8888)
                # server.start(0) # Not works with multiprocessing
                print(db)
                ioloop.IOLoop.current().start()
                print('-' * 20)
        except Exception as ex:
            print(ex)

# if __name__ == '__main__':
#     ChallengeServer.start()

ChallengeServer().start()
