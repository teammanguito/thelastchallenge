from flask import render_template
from app import app

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html')
