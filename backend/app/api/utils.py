from datetime import date, datetime
from tornado import web, escape, ioloop, httpclient, gen

import json


class Utils():
    @staticmethod
    def genericResponse(data, status, errorMessage = ''):
        return {
            'status': status,
            'error': errorMessage,
            'result': data
        }

    @staticmethod
    def checkValue(value, controltype, empty = False, keyname = ''):
        """
        Realiza un control/validación del valor de la variable, según el controltype.
        sí, key es distinto de blank, retorna un dict con keyname como key:valor

        """
        try:
            if isinstance(value, controltype):
                if empty and controltype == str:
                    if value == '':
                        return None

                if keyname != '':
                    return {
                        keyname: value
                    }
                else:
                    return value
            else:
                return None
        
        except TypeError as type_error:
            print('El valor {} no puede ser convertido a {}.'.format(str(value), str(controltype)))
            return None

        except Exception as ex:
            print(ex)
            return None
    @staticmethod
    def json_date_serial(obj):
        """JSON serializer for one object not serializable by default json code"""

        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        raise TypeError ("Type %s not serializable" % type(obj))
    @staticmethod
    def json_serializer(obj):
        """JSON serializer for object not serializable by default json code"""
        return json.loads(json.dumps(obj, indent=4, sort_keys=True, default=str))

class BaseHandler(web.RequestHandler):
    """
    Permite la activación del CORS en la API a través de un extender/modificar parametros en 'BaseHandler'
    """
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "GET,POST")
        self.set_header(
            "Access-Control-Allow-Headers",
            "Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, X-Requested-By, If-Modified-Since, X-File-Name, Cache-Control"
        )
