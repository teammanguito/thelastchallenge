# -*- coding: utf-8 -*-
from datetime import date, datetime
from tornado import web, escape, ioloop, httpclient, gen, escape
from concurrent.futures import ThreadPoolExecutor
from tornado.concurrent import run_on_executor
import json
from app.api.utils import Utils, BaseHandler
from urllib.parse import urlsplit, parse_qs

thread_pool = ThreadPoolExecutor(4)

class LoginHandler(BaseHandler):
    SUPPORTED_METHODS = ('POST')
    _thread_pool = thread_pool
    COLLECTION_NAME = 'users'

    @gen.coroutine
    def post(self, id):
        payload = json.loads(self.request.body)
        if Utils().checkValue(payload, dict) is None:
            self.set_status(400) 
            self.write(
                {
                    'error': 'payload is not valid.',
                    'code': 400
                }
            )
            return
        
        username = Utils().checkValue(payload.get('name', None), str, True)
        password = Utils().checkValue(payload.get('password', None), str, True)

        if username is None or password is None:
            self.set_status(400)
            response = Utils().genericResponse([], 400, 'payload is not valid.')
            self.write(response)
            return
        userObject = []
        db = self.settings['db']
        userCursor = db.get_collection(
            self.COLLECTION_NAME
        ).find({
            'name': username,
            'password': password,
            'enable': 1.0
        })
        while (yield userCursor.fetch_next):
            aux = userCursor.next_object()
            aux['bio'] = aux['bio'].encode('ascii', 'ignore').decode('utf-8')
            aux.pop('password', None)
            # print(aux.values())
            userObject.append(Utils().json_serializer(aux))
        userCursor.close()
        result = Utils().genericResponse(userObject[0], 200, '') #escape.json_decode(userObject)

        self.set_status(200)
        self.write(result)
