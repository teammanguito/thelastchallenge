from datetime import date, datetime
from tornado import web, escape, ioloop, httpclient, gen
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from tornado.concurrent import run_on_executor
import json
from app.api.utils import BaseHandler, Utils # json_date_serial, json_serializer
# from urllib3.util.url import parse_url
from urllib.parse import urlsplit, parse_qs

thread_pool = ThreadPoolExecutor(10)
VALID_LIST = ['typeTxn', 'month', 'day']
class ClientTransactionHandler(BaseHandler):
    SUPPORTED_METHODS = ('GET', 'POST', 'DELETE')
    
    _thread_pool = thread_pool
    COLLECTION_NAME = 'transactions'
    @gen.coroutine 
    def get(self, id):
        db = self.settings['db']
        test : print(dir(self.request))

        query = {}

        query['init'] = self.get_query_arguments('init')[0]
        query['end'] = self.get_query_arguments('end')[0]
        query['txnType'] = self.get_query_arguments('txnType')[0]
        query['groupby'] = self.get_query_arguments('groupby')[0]
        query['limit'] = self.get_query_arguments('limit')[0]
        pipeline, error = yield self.createAggregateFilter(id, query)
        if query['groupby'] != '':
            params = query['groupby'].split(',')
            for i in range(0, len(params)):
                if params[i].strip() not in VALID_LIST:
                    response = {  
                        'status': 400, 
                        'error': 'invalid "{}" in groupby param'.format(params[i]),
                        'result': []
                    }
                    self.set_status(400)
                    self.write(response)
        if pipeline is None:
            response = {  
                'status': 400, 
                'error': error,
                'result': []
            }
            self.set_status(400)
        else:
            result = []
            # print(pipeline)
            cursor = db.get_collection(self.COLLECTION_NAME).aggregate(pipeline)
            while (yield cursor.fetch_next):
                doc = cursor.next_object()
                result.append(doc)
            cursor.close()
            if query['groupby'] != '':
                result = yield self.groupByFilter(result, query['groupby'])
            response = {
                'status': 200,
                'error': error,
                'pipeline': Utils().json_serializer(pipeline),
                'result': Utils().json_serializer(result)
            }
            self.set_status(200)
        self.write(response)
        # self.finish()
    @run_on_executor(executor="_thread_pool")
    def groupByFilter(self, data: dict, options: str):
        params = options.split(',')
        # se valida antes para optimizar los llamados a base de datos.
        # for i in range(0, len(params)):
        #     if params[i].strip() not in VALID_LIST:
        #         return None, 'invalid "{}" in groupby param'.format(params[i])
        params.append('txn')
        lenParams = len(params)
        print(lenParams)
        df = pd.DataFrame.from_dict(data)
        result = df[params].groupby(params[0:lenParams-1]).mean()
        del df
        aux = result.to_dict('split')
        columns = aux['columns']
        data = []
        for i in range(0, len(aux['index'])):
            auxdata = {}
            for x in range(0, len(columns)):
                auxdata[columns[x]] = aux['data'][i][x]
            for x in range(0, len(params)-1):
                auxdata[params[x]] = aux['index'][i][x]
            print(auxdata)
            data.append(auxdata)
        return data

    @run_on_executor(executor="_thread_pool")
    def createAggregateFilter(self, id: int, queryParams: dict):
        """ Crea filtro para la aggregate function de transactions collection
        """
        try:
            limit = self.settings['limit']
            existId = True if isinstance(id, int) or id is None else False
            countParams = 0
            txn = None
            clientId = None
            fecha = {'fecha': {}}
            if sum(map(len, queryParams.values())) == 0 and not existId:
                raise('invalid invoke. id and query parameters is not valid. Check and try again.')

            if queryParams.get('init', None) is not None:

                init = datetime.strptime(queryParams.get('init'), '%Y-%m-%d')
                countParams += 1

            if queryParams.get('end', None) is not None:
                end = datetime.strptime(queryParams.get('end'), '%Y-%m-%d')
                countParams += 1

            if queryParams.get('txnType', None) is not None:
                if queryParams.get('txnType', None) in ['i','e','a']:
                    txnType = queryParams.get('txnType')
                    countParams += 1
            
            try:
                if isinstance(int(id), int):
                    clientId = { 'id': int(id) }
                    countParams += 1
            except:
                clientId = None
            if queryParams.get('limit', None) is not None:
                try:
                    #if isinstance(int(queryParams.get('limit')), int): # not works
                    if type(int(queryParams.get('limit'))) == int:
                        limit = int(queryParams.get('limit'))
                except:
                    return None, 'limit is valid.'
            print(limit)
            if countParams == 0:
                raise('In invoke not exists valid parameters. reject execution.')
            # { score: { $gt: 70, $lt: 90 } }

            if init or end:
                if init:
                    fecha['fecha']['$gt'] = init # queryParams.get('init')
                if end:
                    fecha['fecha']['$lt'] = end # queryParams.get('end')
            print(fecha)
            if txnType == 'i':
                txn = {
                    'txn': {
                        '$gte': 0
                    }
                }

            elif txnType == 'e':
                txn = {
                    'txn': {
                        '$lt': 0
                    }
                }
            pipelineMatch = []
            if isinstance(clientId, dict):
                pipelineMatch.append(clientId)
            
            if isinstance(fecha, dict):
                pipelineMatch.append(fecha)
            
            if isinstance(txn, dict):
                pipelineMatch.append(txn)
            
            pipeline = [{
                '$match': {
                    '$and': pipelineMatch
                }
            }, {
                '$project': {
                    '_id': 0,
                    'fecha': 1,
                    'fechaStr': 1,
                    'month': 1,
                    'day': 1,
                    'typeTxn': 1,
                    'txn': 1
                }
            },{
                '$limit': limit
            }]
            return pipeline, ''
        except TypeError as tex:
            print(tex)
            return None, tex
        except Exception as ex:
            print(ex)
            return None, ex
