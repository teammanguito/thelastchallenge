import pandas as pd

class loadDataBase:

    df = None
    location = 'app/services/hdf/clientDB.h5'


    def load(self):
        self.df = pd.read_hdf(self.location)

    def retrieve(self):
        if self.df is None:
           self.load()
        return self.df.to_dict('records')
