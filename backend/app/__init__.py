import os
from flask import Flask
import connexion
from connexion.resolver import RestyResolver

from config import appConfig
config = appConfig()

app = connexion.App(__name__, specification_dir='./')
# Read the swagger.yml file to configure the endpoints
app.add_api('swagger/services.yml', resolver=RestyResolver('api'))
# app = Flask(__name__)
# app.secret_key = os.urandom(24)
app.debug = True if config.getConfigByEnvironment('currentEnv') == 'debug' else False

from app import index